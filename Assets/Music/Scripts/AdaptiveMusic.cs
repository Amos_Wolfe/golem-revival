﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

/// <summary>
/// Fades in music based on the tension in the game.
/// 
/// Has one main track, and three overlap tracks(idle,suspense,danger)
/// </summary>
public class AdaptiveMusic : MonoBehaviour
{

    // -----------------------------
    // INSTANCING
    // -----------------------------

    private static AdaptiveMusic _instance;
    public static AdaptiveMusic Instance
    {
        get
        {
            if (!_instance)
                _instance = GameObject.FindObjectOfType<AdaptiveMusic>();

            return _instance;
        }
        private set { }
    }

    // -----------------------------
    // CONSTANTS
    // -----------------------------

    public const int STATE_COUNT = 3;
    public enum MUSIC_STATE { Idle, Suspense, Danger };
    public static readonly string[] STATE_NAMES = { "Idle", "Suspense", "Danger" };

    public const float MAX_VOLUME = 0;
    public const float MIN_VOLUME = -80;

    public const float BPS = 0.6f;
    public const float ONE_BAR = BPS * 4;




    // -----------------------------
    // VARIABLES
    // -----------------------------

    [SerializeField]
    AudioMixer masterMixer;

    [SerializeField]
    AudioSource endSource;
    
    MUSIC_STATE musicState = MUSIC_STATE.Idle;
    bool blending = false;

    float[] blendStartVolumes = new float[STATE_COUNT];

    bool endMusic = false;


    void Start()
    {
        StartCoroutine(FadeInMusic());
        SetMusicState(MUSIC_STATE.Idle);
    }

    IEnumerator FadeInMusic()
    {
        float fadeTime = ONE_BAR/2;

        // fade in
        float t = 0;
        float startTime = Time.time;
        float timePassed = 0;
        while (timePassed < fadeTime)
        {
            timePassed += Time.unscaledDeltaTime;
            t = Mathf.Min(timePassed / fadeTime, 1);

            float blendedVolume = Mathf.Lerp(-40.0f, 0.0f, t);
            masterMixer.SetFloat("MusicVolume", blendedVolume);

            yield return 1;
        }
    }

    public void SetMusicState(string stateName)
    {
        for(int i = 0; i < STATE_NAMES.Length; i++)
        {
            if(stateName == STATE_NAMES[i])
            {
                SetMusicState((MUSIC_STATE)i);
                break;
            }
        }
    }

    public void SetMusicState(MUSIC_STATE newState)
    {
        if (!endMusic)
        {
            if (musicState != newState)
            {
                musicState = newState;
                StartCoroutine(BlendToState(newState));
            }
        }
    }

    IEnumerator BlendToState(MUSIC_STATE newState)
    {
        // wait for blending to finish
        while (blending)
            yield return 1;

        blending = true;

        // wait for next beat
        float nextBeat = Time.time + Time.time % BPS + BPS;
        yield return new WaitForSeconds(nextBeat - Time.time);

        // get starting volumes
        for (int i = 0; i < STATE_NAMES.Length; i++)
        {
             masterMixer.GetFloat(STATE_NAMES[i] + "Volume", out blendStartVolumes[i]);
        }

        // start the cross fade over a beat
        float t = 0;
        float startTime = Time.time;
        float timePassed = 0;
        while(timePassed < BPS)
        {
            timePassed += Time.unscaledDeltaTime;
            t = Mathf.Min( timePassed / BPS, 1);

            for (int i = 0; i < STATE_NAMES.Length; i++)
            {
                float blendedVolume = 0;

                if (newState.ToString() == STATE_NAMES[i])
                    blendedVolume = Mathf.Lerp(blendStartVolumes[i], MAX_VOLUME, t);
                else
                    blendedVolume = Mathf.Lerp(blendStartVolumes[i], MIN_VOLUME, t);

                masterMixer.SetFloat(STATE_NAMES[i] + "Volume", blendedVolume);
            }

            yield return 1;
        }

        blending = false;
    }

    /// <summary>
    /// Lowers the music for a period of time
    /// </summary>
    public void LowerMusic()
    {
        if(!endMusic)
            StartCoroutine(LowerMusicCoroutine());
    }

    IEnumerator LowerMusicCoroutine()
    {
        float fadeTime = 2.5f;

        // fade out
        float t = 0;
        float startTime = Time.time;
        float timePassed = 0;
        while (timePassed < fadeTime)
        {
            timePassed += Time.unscaledDeltaTime;
            t = Mathf.Min(timePassed / fadeTime, 1);

            float blendedVolume = Mathf.Lerp(0.0f, -20.0f, t);
            masterMixer.SetFloat("MusicVolume", blendedVolume);

            yield return 1;
        }


        timePassed = 0;
        while (timePassed < fadeTime)
        {
            timePassed += Time.unscaledDeltaTime;
            t = Mathf.Min(timePassed / fadeTime, 1);

            float blendedVolume = Mathf.Lerp(-20.0f, 0.0f, t);
            masterMixer.SetFloat("MusicVolume", blendedVolume);

            yield return 1;
        }
    }


    public void PlayerEndMusic()
    {
        if (!endMusic)
        {
            endMusic = true;
            StartCoroutine(EndMusicCoroutine());
        }
    }

    IEnumerator EndMusicCoroutine()
    {
        // wait for blending to finish
        while (blending)
            yield return 1;

        blending = true;

        // wait for next bar
        float nextBar = Time.time + Time.time % ONE_BAR + ONE_BAR;
        yield return new WaitForSeconds(nextBar - Time.time);

        yield return new WaitForEndOfFrame();

        // fade out
        float fadeTime = ONE_BAR;
        endSource.PlayOneShot(Resources.Load<AudioClip>("Music/MainThemeEnd"));
        float t = 0;
        float timePassed = 0;
        while (timePassed < fadeTime)
        {
            timePassed += Time.unscaledDeltaTime;
            t = Mathf.Min(timePassed / fadeTime, 1);
            
            float blendedVolume = Mathf.Lerp(-80.0f, 0.0f, t);
            masterMixer.SetFloat("EndVolume", blendedVolume);

            yield return 1;
        }
        
        timePassed = 0;
        while (timePassed < fadeTime)
        {
            timePassed += Time.unscaledDeltaTime;
            t = Mathf.Min(timePassed / fadeTime, 1);

            float blendedVolume = Mathf.Lerp(0.0f, -80.0f, t);
            masterMixer.SetFloat("MusicVolume", blendedVolume);

            yield return 1;
        }
    }

}
