﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The entire resource display, showing amounts for all resource.
/// </summary>
public class ResourceDisplay : MonoBehaviour
{

	void Awake()
    {
        SingleResourceDisplay displayPrefab =
                Resources.Load<SingleResourceDisplay>("SingleResourceDisplay");

        float buttonHeight = 1.0f / ((float)ResourceManager.RESOURCE_COUNT-1);
        float buttonY = 1 - buttonHeight;

        for (int i = 1; i < ResourceManager.RESOURCE_COUNT; i++)
        {
            SingleResourceDisplay newDisplay =
                (SingleResourceDisplay)Instantiate(displayPrefab);

            RectTransform newDisplayRect = newDisplay.GetComponent<RectTransform>();

            newDisplay.transform.parent = this.transform;
            newDisplay.transform.localScale = Vector3.one;

            newDisplayRect.anchorMin = new Vector2(0, buttonY);
            newDisplayRect.anchorMax = new Vector2(1, buttonY + buttonHeight);
            newDisplayRect.offsetMin = new Vector2(4,4);
            newDisplayRect.offsetMax = new Vector2(-4, -4);

            newDisplay.Init((ResourceManager.RESOURCE_TYPE)i);

            buttonY -= buttonHeight;
        }
    }

}
