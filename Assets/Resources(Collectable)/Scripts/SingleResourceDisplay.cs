﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Displays the amount for a single resource
/// </summary>
public class SingleResourceDisplay : MonoBehaviour
{

    // -----------------------------
    // VARIABLES
    // -----------------------------

    [SerializeField]
    Image resourceIcon;

    [SerializeField]
    Text resourceAmount;

    ResourceManager.RESOURCE_TYPE resourceType;



    // -----------------------------
    // INITIALISATION
    // -----------------------------

    public void Init(ResourceManager.RESOURCE_TYPE resourceType)
    {
        this.resourceType = resourceType;

        resourceIcon.sprite = Resources.Load<Sprite>("ResourceIcons/" + resourceType.ToString());
    }

    

    // -----------------------------
    // UPDATE
    // -----------------------------

    void Update()
    {
        resourceAmount.text = ResourceManager.Instance.GetResourceAmount(resourceType).ToString();
    }

}
