﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Handles the collection and storage of resources
/// </summary>
public class ResourceManager : MonoBehaviour
{

    // -----------------------------
    // INSTANCING
    // -----------------------------

    private static ResourceManager _instance;
    public static ResourceManager Instance
    {
        get
        {
            if (!_instance)
                _instance = GameObject.FindObjectOfType<ResourceManager>();

            return _instance;
        }
        private set { }
    }

    // -----------------------------
    // CONSTANTS
    // -----------------------------

    public const int GOLEM_SHELL_MATERIAL_REQUIREMENT = 5;

    public const string RESOURCE_PATH = "Resource";

    /// <summary>
    /// More resources will be spawned when the resource count for a resource
    /// type is below this.
    /// </summary>
    public const int RESOURCE_TARGET_COUNT = 30;

    public const int RESOURCE_RESPAWN_RATE = 1;

    public const int RESOURCE_COUNT = 9;

    public const float RESOUCE_MIN_DISTANCE = 7;

    // TODO make a list of forbidden resources (ones that cannot be spent)
    // TODO have a list of resources that can used for golems
    public enum RESOURCE_TYPE
    { None, People, Food, Metal, Salvage, Arcane, Wood, Rock, Water };

    public static readonly string[] RESOURCE_NAMES =
        {"None","People","Food","Metal","Salvage","Arcane","Wood","Rock","Water"};

    // defines which resource types can be used to construct a golem
    public static readonly bool[] GOLEM_RESOURCES
        = { false, false, false, true, true, true, true, true, true };

    
    public static readonly int[] RESOURCE_INITIAL 
        = {0,10,5,5,5,5,5,5,5};

    // TO DO: place this in some global constants class.
    public const float MAP_SIZE = 50;

    public const int RESOURCE_PILE_COUNT = 3;
    public const float RESOURCE_PILE_SPREAD = 2.0f;

    public const int PEOPLE_START_AMOUNT = 10;

    // -----------------------------
    // VARIABLES
    // -----------------------------

    [SerializeField]
    Transform resourceAreaInfo;

    Resource resourcePrefab;
    
    List<List<Resource>> worldResourceList = new List<List<Resource>>();

    List<List<Vector2>> resourcePileList = new List<List<Vector2>>();

    int[] collectedResourceList = new int[RESOURCE_COUNT];

    // -----------------------------
    // INITIALISATION
    // -----------------------------

    void Awake()
    {
        resourcePrefab = Resources.Load<Resource>(RESOURCE_PATH);

        for (int i = 0; i < RESOURCE_COUNT; i++)
            worldResourceList.Add(new List<Resource>());

        for (int i = 0; i < RESOURCE_COUNT; i++)
            resourcePileList.Add(new List<Vector2>());

        // populate the pile lists
        Transform currentTransform = null;
        List<string> resourceNameList = RESOURCE_NAMES.ToList();
        for (int i = 0; i < resourceAreaInfo.childCount; i++)
        {
            currentTransform = resourceAreaInfo.GetChild(i);

            int resIndex = resourceNameList.FindIndex(x => x == currentTransform.name);
            if(resIndex != -1)
            {
                resourcePileList[resIndex].Add(currentTransform.position);
            }
        }
    }
    
    void Start()
    {
        for (int i = 0; i < 50; i++)
        {
            CreateRandomResource();
        }

        // have a base amount of resources
        for(int i = 0; i < RESOURCE_INITIAL.Length; i++)
            AddResource((RESOURCE_TYPE)i, RESOURCE_INITIAL[i]);

        StartCoroutine(ResourceSpawnUpdate());
    }



    // -----------------------------
    // GET/SET
    // -----------------------------

    public int GetResourceAmount(RESOURCE_TYPE resType)
    {
        return collectedResourceList[(int)resType];
    }

    // -----------------------------
    // UPDATE
    // -----------------------------

    void Update()
    {
        if (collectedResourceList[(int)RESOURCE_TYPE.People] <= 0)
        {
            GameStateManager.Instance.Lose();
        }
    }

    IEnumerator ResourceSpawnUpdate()
    {
        while(true)
        {
            // if a resource pile is below the threshold
            for(int i = 1; i < RESOURCE_COUNT; i++)
            {
                // start spawning more resources
                if(worldResourceList[i].Count < RESOURCE_TARGET_COUNT)
                {
                    CreateResourcePileAtRandomPosition(
                        (RESOURCE_TYPE)i, RESOURCE_RESPAWN_RATE);
                }
            }

            yield return new WaitForSeconds(1);
        }
    }

    public void CreateRandomResource()
    {
        CreateResourcePileAtRandomPosition(
            (RESOURCE_TYPE)Random.Range(0, RESOURCE_COUNT),
            RESOURCE_PILE_COUNT);
    }

    public void CreateResourcePileAtRandomPosition(RESOURCE_TYPE resType, int amount)
    {
        // use a random stockpile
        int resIndex = (int)resType;
        List<Vector2> posList = resourcePileList[resIndex];
        if (posList.Count > 0)
        {
            Vector2 randomSpawn = posList[Random.Range(0, posList.Count)];
            CreateResourcePile((RESOURCE_TYPE)resIndex, randomSpawn, amount);
        }
        else
        {
            // use a random position if a stockpile cannot be found
            float spawnSize = MAP_SIZE - RESOURCE_PILE_SPREAD;
            Vector2 randomSpawn = new Vector2(
                Random.Range(-spawnSize / 2, spawnSize / 2),
                Random.Range(-spawnSize / 2, spawnSize / 2));
            CreateResourcePile((RESOURCE_TYPE)resIndex, randomSpawn, amount);
        }
    }

    public void CreateResourcePile(RESOURCE_TYPE resType, Vector2 spawnPos, int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            Vector2 randomPos = spawnPos;
            randomPos.x += Random.Range(-RESOURCE_PILE_SPREAD, RESOURCE_PILE_SPREAD);
            randomPos.y += Random.Range(-RESOURCE_PILE_SPREAD, RESOURCE_PILE_SPREAD);

            if(randomPos.magnitude > RESOUCE_MIN_DISTANCE)
                CreateResource(resType, randomPos);
        }
    }

    public void CreateResource(RESOURCE_TYPE resType, Vector2 spawnPos)
    {
        Resource newResource = (Resource)Instantiate(resourcePrefab, spawnPos, Quaternion.identity);
        newResource.Init(resType);


        int sortedIndex = 
            worldResourceList[(int)resType].FindIndex
            (x => x.GetDistanceFromVillage() > newResource.GetDistanceFromVillage());

        if (sortedIndex == -1)
        {
            worldResourceList[(int)resType].Add(newResource);
        }
        else
        {
            worldResourceList[(int)resType].Insert(sortedIndex, newResource);
        }
    }

    public void RemoveResource(Resource targetResource)
    {
        worldResourceList[(int)targetResource.GetResourceType()].Remove(targetResource);
        DestroyObject(targetResource.gameObject);
    }
    

    public Resource FindResourceNearPosition(RESOURCE_TYPE resType, Vector2 targetPos)
    {
        Collider2D[] colliderList = Physics2D.OverlapCircleAll(targetPos, RESOURCE_PILE_SPREAD*2, CommandHandler.SELECTABLE_LAYER);
        for(int i = 0; i < colliderList.Length; i++)
        {
            Resource resourceFound = colliderList[i].GetComponent<Resource>();
            if(resourceFound && resourceFound.GetResourceType() == resType)
            {
                return resourceFound;
            }
        }

        // try to find any resource with the same type
        List<Resource> similarResourceList = worldResourceList[(int)resType];
        if (similarResourceList.Count > 0)
            return similarResourceList[0];

        return null;
    }

    public bool MeetsRequirements(int amount, bool golemResourcesOnly = false)
    {
        // check if all resources are at least 1
        for (int i = 1; i < RESOURCE_COUNT; i++)
        {
            bool resourceBelowAmount =
                GetResourceAmount((RESOURCE_TYPE)i) < amount;
            if (golemResourcesOnly)
            {
                if (resourceBelowAmount && GOLEM_RESOURCES[i])
                    return false;
            }
            else if (resourceBelowAmount)
            {
                if (resourceBelowAmount)
                    return false;
            }
        }

        return true;
    }


    // -----------------------------
    // RESOURCE MANAGEMENT / DISPLAY
    // -----------------------------

    public void AddResource(RESOURCE_TYPE resType, int amount)
    {
        collectedResourceList[(int)resType] += amount;
        //Debug.Log(collectedResourceList[(int)resType]);
    }

    public bool SpendResource(RESOURCE_TYPE resType, int amount)
    {
        if (collectedResourceList[(int)resType] - amount >= 0)
        {
            collectedResourceList[(int)resType] -= amount;
            return true;
        }

        return false;
    }

    /*
    // spends all unforbidden resources with the same amount
    public bool SpendAllResources(int amount)
    {
        for(int i = 1; i < collectedResourceList.Length; i++)
        {

        }

        return true;
    }
    */

    public void GetEverything()
    {
        for (int i = 1; i < collectedResourceList.Length; i++)
            collectedResourceList[i] += 10000;
    }

    /*
    void OnGUI()
    {

        string resourcesString = "";

        float iconSize = 128;

        for(int i = 1; i < RESOURCE_COUNT; i++)
        {
            string resourceName = ((RESOURCE_TYPE)i).ToString();
            resourcesString += resourceName + ": " + collectedResourceList[i] + "\n";

            Texture2D uiIcon = Resources.Load<Texture2D>("ResourceIcons/" + (RESOURCE_TYPE)i);

            GUI.DrawTexture(new Rect(20, 20 + i * iconSize, iconSize, iconSize), uiIcon);
        }

        //GUI.Label(new Rect(20, 20, 500, 800), resourcesString);
    }
    */
}
