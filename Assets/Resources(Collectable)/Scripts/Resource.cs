﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A collectable resource
/// </summary>
public class Resource : MonoBehaviour
{

    // -----------------------------
    // CONSTANTS
    // -----------------------------
    
    // TODO replace with image later
    public static readonly Color[] RESOURCE_COLOURS =
    {
        Color.black,
        new Color(1,0.8f,0.8f),// people
        new Color(1,0.5f,0.5f),// food
        Color.black, // metal
        Color.blue, // salvage
        new Color(0.5f,0,0.5f), // arcane
        new Color(0.5f,0.25f,0), // wood
        Color.gray // rock
    };



    // -----------------------------
    // VARIABLES
    // -----------------------------

    [SerializeField]
    SpriteRenderer visualSprite;

    ResourceManager.RESOURCE_TYPE resourceType;

    float distanceFromVillage = 0;

    

    // -----------------------------
    // INITIALISATION
    // -----------------------------

    public void Init(ResourceManager.RESOURCE_TYPE resType)
    {
        // TODO replace with image later
        //visualSprite.color = RESOURCE_COLOURS[(int)resType];
        visualSprite.sprite = Resources.Load<Sprite>("ResourceIcons/" + 
            resType.ToString());

        distanceFromVillage = transform.position.magnitude;

        this.resourceType = resType;
    }


    // -----------------------------
    // GET / SET
    // -----------------------------

    public ResourceManager.RESOURCE_TYPE GetResourceType()
    {
        return resourceType;
    }

    public float GetDistanceFromVillage()
    {
        return distanceFromVillage;
    }



    // -----------------------------
    // UPDATE
    // -----------------------------

    public void RemoveResource()
    {
        ResourceManager.Instance.RemoveResource(this);
    }

}
