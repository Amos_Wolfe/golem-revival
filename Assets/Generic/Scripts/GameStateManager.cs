﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

/// <summary>
/// Controls win and lose conditions
/// </summary>
public class GameStateManager : MonoBehaviour
{

    // -----------------------------
    // INSTANCING
    // -----------------------------

    private static GameStateManager _instance;
    public static GameStateManager Instance
    {
        get
        {
            if (!_instance)
                _instance = GameObject.FindObjectOfType<GameStateManager>();

            return _instance;
        }
        private set { }
    }

    // -----------------------------
    // CONSTANTS
    // -----------------------------

    //public const float RESTART_TIME = 30;
    public enum GAME_STATE { Playing, Winning, Losing };


    // -----------------------------
    // VARIABLES
    // -----------------------------

    [SerializeField]
    GameObject winUI;

    [SerializeField]
    GameObject loseUI;

    [SerializeField]
    GameObject exitDialog;

    [SerializeField]
    GameObject pauseText;

    GAME_STATE gameState = GAME_STATE.Playing;


    // -----------------------------
    // UPDATE
    // -----------------------------

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            exitDialog.SetActive(true);
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
                pauseText.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                pauseText.SetActive(false);
            }
        }
    }

    public void Win()
    {
        if(gameState == GAME_STATE.Playing)
        {
            gameState = GAME_STATE.Winning;
            AdaptiveMusic.Instance.PlayerEndMusic();
            winUI.SetActive(true);
            //Invoke("Restart", RESTART_TIME);
        }
    }

    public void Lose()
    {
        if (gameState == GAME_STATE.Playing)
        {
            gameState = GAME_STATE.Losing;
            AdaptiveMusic.Instance.PlayerEndMusic();
            loseUI.SetActive(true);
            //Invoke("Restart", RESTART_TIME);
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Quit()
    {
        Application.Quit();
    }

}
