﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InfoTextDisplay : MonoBehaviour
{

    // -----------------------------
    // INSTANCING
    // -----------------------------

    private static InfoTextDisplay _instance;
    public static InfoTextDisplay Instance
    {
        get
        {
            if (!_instance)
                _instance = GameObject.FindObjectOfType<InfoTextDisplay>();

            return _instance;
        }
        private set { }
    }



    // -----------------------------
    // VARIABLES
    // -----------------------------

    Text thisText;



    // -----------------------------
    // INITIALISATION
    // -----------------------------

    void Awake()
    {
        thisText = GetComponent<Text>();
    }

    // -----------------------------
    // UPDATE
    // -----------------------------

    public void DisplayText(string newText)
    {
        thisText.text = newText;
    }

}
