﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Handles selecting units and buildings, as well as issuing commands.
/// </summary>
public class CommandHandler : MonoBehaviour
{

    // -----------------------------
    // INSTANCING
    // -----------------------------

    private static CommandHandler _instance;
    public static CommandHandler Instance
    {
        get
        {
            if (!_instance)
                _instance = GameObject.FindObjectOfType<CommandHandler>();

            return _instance;
        }
        private set { }
    }

    // -----------------------------
    // CONSTANTS
    // -----------------------------

    public static readonly LayerMask SELECTABLE_LAYER = 1 << LayerMask.NameToLayer("Selectable");
    public const float MOVE_UNIT_SPREAD = 0.7f;
    public const int MIN_SELECT_BOX_SIZE = 5;


    [SerializeField]
    RectTransform dragRectangle;

    bool mouseDrag = false;
    Vector2 mouseStart;

    GolemSpawner golemSpawn;

    List<Commandable> unitsSelected = new List<Commandable>();

    AudioSource thisAudio;


    // -----------------------------
    // INITIALISATION
    // -----------------------------


    void Awake()
    {
        golemSpawn = GameObject.FindObjectOfType<GolemSpawner>();
        thisAudio = gameObject.AddComponent<AudioSource>();
    }



    // -----------------------------
    // UPDATE
    // -----------------------------

    public IEnumerator PlayCommandSound()
    {
        // play the command sound
        thisAudio.PlayOneShot(
            Resources.Load<AudioClip>("Sounds/Golum Command_" + Random.Range(2, 4)), 0.2f);

        // play three random footstep sounds
        float currentVolume = 0.3f;
        for (int i = 0; i < 3; i++)
        {
            thisAudio.PlayOneShot(
                Resources.Load<AudioClip>("Sounds/Golem_FS_" + Random.Range(1, 4)), currentVolume);
            currentVolume -= 0.1f;
            yield return new WaitForSeconds(0.2f);
        }
    }

    void Update()
    {
        // left click
        if(Input.GetMouseButtonDown(0))
        {
            mouseDrag = true;
            mouseStart = Input.mousePosition;
        }
        else if(Input.GetMouseButton(0))
        {
            dragRectangle.gameObject.SetActive(true);

            // TODO show box
            Vector2 mouseEnd = Input.mousePosition;

            Vector2 minCornerScreen = Vector2.Min(mouseStart, mouseEnd);
            Vector2 maxCornerScreen = Vector2.Max(mouseStart, mouseEnd);

            dragRectangle.offsetMin = minCornerScreen;
            dragRectangle.offsetMax = maxCornerScreen;
        }
        else if(Input.GetMouseButtonUp(0))
        {
            mouseDrag = false;

            Vector2 mouseEnd = Input.mousePosition;

            Vector2 minCornerScreen = Vector2.Min(mouseStart, mouseEnd);
            Vector2 maxCornerScreen = Vector2.Max(mouseStart, mouseEnd);

            if ((maxCornerScreen - minCornerScreen).magnitude > MIN_SELECT_BOX_SIZE)
            {
                DragSelect(
                Camera.main.ScreenToWorldPoint(minCornerScreen),
                Camera.main.ScreenToWorldPoint(maxCornerScreen)
                );
            }
            else
            {
                SingleSelect(Camera.main.ScreenToWorldPoint(minCornerScreen));
            }

            dragRectangle.gameObject.SetActive(false);
        }


        // right click
        if (Input.GetMouseButtonDown(1))
        {
            RightClick(Input.mousePosition);
        }
    }

    public void RemoveFromSelection(Commandable targetCommandable)
    {
        unitsSelected.Remove(targetCommandable);
    }

    void SingleSelect(Vector2 clickPos)
    {
        DeselectAll();

        Collider2D[] colliderList = Physics2D.OverlapPointAll(clickPos, SELECTABLE_LAYER);
        for(int i = 0; i < colliderList.Length; i++)
        {
            Commandable commandableFound = colliderList[i].GetComponentInParent<Commandable>();
            if(commandableFound)
            {
                commandableFound.Selected();
                unitsSelected.Add(commandableFound);
            }
        }
    }

    void DragSelect(Vector2 minCorner, Vector2 maxCorner)
    {
        DeselectAll();

        // get units
        golemSpawn.GetGolemsInArea(minCorner, maxCorner, ref unitsSelected);
    }

    void DeselectAll()
    {
        for (int i = 0; i < unitsSelected.Count; i++)
        {
            unitsSelected[i].Deselected();
        }
        unitsSelected.Clear();
    }

    bool RightClick(Vector2 clickPos)
    {
        Vector2 clickWorldPos = Camera.main.ScreenToWorldPoint(clickPos);

        if(unitsSelected.Count > 0)
            StartCoroutine(PlayCommandSound());

        Collider2D colliderFound = Physics2D.OverlapPoint(clickWorldPos, SELECTABLE_LAYER);
        if(colliderFound)
        {
            // go to enemy
            Slaver slaverFound = colliderFound.GetComponentInParent<Slaver>();
            if (slaverFound)
            {
                for (int i = 0; i < unitsSelected.Count; i++)
                    unitsSelected[i].ClickAtEnemy(slaverFound);

                return true;
            }

            // go to resource
            Resource resourceFound = colliderFound.GetComponentInParent<Resource>();
            if (resourceFound)
            {
                for (int i = 0; i < unitsSelected.Count; i++)
                    unitsSelected[i].ClickAtResource(resourceFound);

                return true;
            }

            // go to base
            if(colliderFound.tag == "Base")
            {
                for (int i = 0; i < unitsSelected.Count; i++)
                    unitsSelected[i].ClickAtBase();

                return true;
            }
        }

        // go to position
        // (need to spread out positions)
        clickWorldPos.x -= (float)unitsSelected.Count / 2 * MOVE_UNIT_SPREAD;
        for (int i = 0; i < unitsSelected.Count; i++)
        {
            unitsSelected[i].ClickAtPosition(clickWorldPos);
            clickWorldPos.x += MOVE_UNIT_SPREAD;
        }



        return false;
        
    }

}
