﻿using UnityEngine;
using System.Collections;

public class HealthDisplay : MonoBehaviour
{

    [SerializeField]
    RectTransform healthBar;

    float maxHealth;
    float health;

    public void SetMaxHealth(float amount)
    {
        maxHealth = amount;
        health = amount;

        UpdateDisplay();
    }

    public void SetHealth(float amount)
    {
        health = amount;

        UpdateDisplay();
    }

    void UpdateDisplay()
    {
        healthBar.anchorMax = new Vector2(health/maxHealth, 1);
    }

}
