﻿using UnityEngine;
using System.Collections;

public abstract class Commandable: MonoBehaviour
{

    public abstract void Selected();

    public abstract void Deselected();

    public abstract void ClickAtPosition(Vector2 clickWorldPos);

    public abstract void ClickAtEnemy(Slaver targetEnemy);

    public abstract void ClickAtResource(Resource targetResource);

    public abstract void ClickAtBase();

}
