﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Pan with WASD
/// Zoom with mouse wheel
/// </summary>
public class CameraRTS : MonoBehaviour
{

    // -----------------------------
    // INSTANCING
    // -----------------------------

    private static CameraRTS _instance;
    public static CameraRTS Instance
    {
        get
        {
            if (!_instance)
                _instance = GameObject.FindObjectOfType<CameraRTS>();

            return _instance;
        }
        private set { }
    }

    // -----------------------------
    // CONSTANTS
    // -----------------------------

    public const float MOVE_SPEED = 1.5f;

    public const float MAP_SIZE = 50;

    public const float MIN_CAM_SIZE = 2;
    public const float MAX_CAM_SIZE = 10;
    public const float ZOOM_SPEED = 5;

    Vector2 moveVector;

    bool forceMoving = false;

	void Update()
    {
        if (!forceMoving)
        {
            Camera mainCam = Camera.main;

            // pan
            moveVector.x = Input.GetAxisRaw("Horizontal");
            moveVector.y = Input.GetAxisRaw("Vertical");
            transform.position += (Vector3)moveVector * MOVE_SPEED * mainCam.orthographicSize * Time.unscaledDeltaTime;

            // zoom
            float mouseScroll = Input.GetAxis("Mouse ScrollWheel");
            float zoomChange = mouseScroll * ZOOM_SPEED;
            mainCam.orthographicSize = Mathf.Clamp(mainCam.orthographicSize - zoomChange, MIN_CAM_SIZE, MAX_CAM_SIZE);

            // snap to bounds
            Vector3 newPos = mainCam.transform.position;

            float screenHeight = mainCam.orthographicSize;
            float screenWidth = screenHeight * mainCam.aspect;

            newPos.x = Mathf.Clamp(newPos.x, -MAP_SIZE / 2 + screenWidth, MAP_SIZE / 2 - screenWidth);
            newPos.y = Mathf.Clamp(newPos.y, -MAP_SIZE / 2 + screenHeight, MAP_SIZE / 2 - screenHeight);

            mainCam.transform.position = newPos;
        }
    }

    public void MoveToPos(float time, Vector2 targetPos)
    {
        StartCoroutine(MoveToPosCoroutine(time,targetPos));
    }

    IEnumerator MoveToPosCoroutine(float time, Vector2 targetPos)
    {
        forceMoving = true;

        Vector3 newTargetPos = targetPos;
        newTargetPos.z = Camera.main.transform.position.z;

        Vector3 startPos = Camera.main.transform.position;

        Vector3 newPos = startPos;

        // fade out
        float t = 0;
        float startTime = Time.time;
        float timePassed = 0;
        while (timePassed < time)
        {
            timePassed += Time.deltaTime;
            t = Mathf.Min(timePassed / time, 1);

            newPos.x = Mathf.SmoothStep(startPos.x, newTargetPos.x, t);
            newPos.y = Mathf.SmoothStep(startPos.y, newTargetPos.y, t);
            float screenHeight = Camera.main.orthographicSize;
            float screenWidth = screenHeight * Camera.main.aspect;
            newPos.x = Mathf.Clamp(newPos.x, -MAP_SIZE / 2 + screenWidth, MAP_SIZE / 2 - screenWidth);
            newPos.y = Mathf.Clamp(newPos.y, -MAP_SIZE / 2 + screenHeight, MAP_SIZE / 2 - screenHeight);

            Camera.main.transform.position = newPos;

            yield return 1;
        }

        forceMoving = false;
    }

}
