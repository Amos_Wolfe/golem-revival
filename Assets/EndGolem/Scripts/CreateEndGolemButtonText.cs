﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreateEndGolemButtonText : MonoBehaviour
{

    Text thisText;

    void Awake()
    {
        thisText = GetComponent<Text>();
    }

	void Update ()
    {
        if (MeetsRequirements(EndGolem.MAJOR_GOLEM_REQUIREMENT))
        {
            thisText.text = "Start Ritual";
        }
        else
        {
            thisText.text = "Start Ritual \n(Not enough materials)";
        }
    }

    // TODO place in resource manager
    public bool MeetsRequirements(int amount)
    {
        // check if all resources are at least 1
        for (int i = 1; i < ResourceManager.RESOURCE_COUNT; i++)
        {
            if (ResourceManager.Instance.GetResourceAmount((ResourceManager.RESOURCE_TYPE)i) < amount)
            {
                return false;
            }
        }

        return true;
    }

}
