﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class EndGolem : MonoBehaviour, IPointerClickHandler
{

    public const int MAJOR_GOLEM_REQUIREMENT = 100;

    [SerializeField]
    GameObject endGolemUI;

    [SerializeField]
    SpriteRenderer buriedGolem;
    [SerializeField]
    SpriteRenderer unburiedGolem;

    [SerializeField]
    GameObject exclamationMark;

    AudioSource thisAudio;


    void Awake()
    {
        thisAudio = gameObject.AddComponent<AudioSource>();
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if(eventData.button == 0)
        {
            exclamationMark.SetActive(false);
            endGolemUI.SetActive(true);
        }
    }

    public void CloseUI()
    {
        endGolemUI.SetActive(false);
    }


    public void CreateEndGolem()
    {
        if(ResourceManager.Instance.MeetsRequirements(MAJOR_GOLEM_REQUIREMENT))
        {
            Debug.Log("Win Game");
            GameStateManager.Instance.Win();
            unburiedGolem.enabled = true;
            buriedGolem.enabled = false;
            endGolemUI.SetActive(false);

            thisAudio.PlayOneShot( Resources.Load<AudioClip>("Sounds/Golem Life"), 0.7f);
        }
        else
        {
            Debug.Log("Not enough resources");
        }
    }

}
