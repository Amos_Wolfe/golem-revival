﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Creates a set of statistics for a golem based on the resources used.
/// </summary>
public class GolemConstructor : MonoBehaviour
{

    // -----------------------------
    // INSTANCING
    // -----------------------------

    private static GolemConstructor _instance;
    public static GolemConstructor Instance
    {
        get
        {
            if (!_instance)
                _instance = GameObject.FindObjectOfType<GolemConstructor>();

            return _instance;
        }
        private set { }
    }

    // -----------------------------
    // CONSTANTS
    // -----------------------------

    public const float BASE_HP = 120;
    public const float BASE_HP_REGEN = 5;
    public const float BASE_ATTACK = 5;
    public const float BASE_ROF = 0.5f;
    public const float BASE_DEFENCE = 0;
    public const float BASE_MOVEMENT_SPEED = 4.0f;
    public const float BASE_RES_COL_SPEED = 2;
    public const float BASE_RES_CAP = 3;


    // -----------------------------
    // UPDATE
    // -----------------------------

    public void CreateGolem(Vector3 position, int[] resourceAmounts)
    {
        //foreach (int resource in resourceAmounts)
            //Debug.Log(resource);

        //float food = resourceAmounts[(int)ResourceManager.RESOURCE_TYPE.Food];
        float metal = resourceAmounts[(int)ResourceManager.RESOURCE_TYPE.Metal];
        float gears = resourceAmounts[(int)ResourceManager.RESOURCE_TYPE.Salvage];
        float crystals = resourceAmounts[(int)ResourceManager.RESOURCE_TYPE.Arcane];
        float wood = resourceAmounts[(int)ResourceManager.RESOURCE_TYPE.Wood];
        float stone = resourceAmounts[(int)ResourceManager.RESOURCE_TYPE.Rock];
        float water = resourceAmounts[(int)ResourceManager.RESOURCE_TYPE.Water];

        //Debug.Log(metal);

        float hp = BASE_HP + stone + wood + metal;
        float healthRegen = BASE_HP_REGEN + (crystals / 5.0f);
        float attack = BASE_ATTACK + metal;
        float rof = BASE_ROF + Mathf.Min(water,gears) / 5.0f;
        float defence = BASE_DEFENCE + (stone / 10.0f);
        float movementSpeed = BASE_MOVEMENT_SPEED + Mathf.Min(water, crystals);
        float resColSpeed = BASE_RES_COL_SPEED + (gears + metal) / 5.0f;
        int resCap = (int)Mathf.Ceil(BASE_RES_CAP + (metal + stone) / 2.0f);

        /*
        Debug.Log(
            "hp: " + hp + "\n" +
            "regen: " + healthRegen + "\n" +
            "attack: " + attack + "\n" +
            "rof: " + rof + "\n" +
            "def: " + defence + "\n" +
            "speed: " + movementSpeed + "\n" +
            "res collect: " + resColSpeed + "\n" +
            "res capacity: " + resCap);*/

        Golem newGolem = GolemSpawner.Instance.CreateLiveGolem(position);
        newGolem.Init(hp, healthRegen, attack, rof, defence, movementSpeed, 
            resColSpeed, resCap);
    }

}
