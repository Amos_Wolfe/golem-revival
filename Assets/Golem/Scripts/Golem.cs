﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

/// <summary>
/// A selectable and movable golem.
/// </summary>
public class Golem : Commandable, IPointerEnterHandler, IPointerExitHandler
{
    
    // -----------------------------
    // CONSTANTS
    // -----------------------------

    public enum GOLEM_STATE {Idle, MovingToPosition, MovingToResource, TakingResourceToBase, MovingToEnemy, Attacking, Dead};
    
    public const float DRAW_START_POS = -50;
    public const float DRAW_MAP_SIZE = 100;

    public const int LAYER_START = 10;
    public const int LAYER_COUNT = 100;

    //public const float MOVE_SPEED = 2;
    //public const float ATTACK_COOLDOWN = 0.5f;
    //public const int ATTACK_DAMAGE = 20;
    public const float ATTACK_RANGE = 0.5f;
    public const float PICKUP_RANGE = 0.5f;

    public const float ATTACK_PUSH = 5;
    public const float PUSH_DECAY = 10;

    // -----------------------------
    // VARIABLES
    // -----------------------------

    private static bool selectHelpShown = false;
    private static bool moveHelpShown = false;
    private static bool resourceHelpShown = false;

    [SerializeField]
    SpriteRenderer selection;
    [SerializeField]
    SpriteRenderer resourceCarrying;
    [SerializeField]
    SpriteRenderer visualSprite;
    //[SerializeField]
    //int maxHealth = 100;
    [SerializeField]
    HealthDisplay healthDisplay;

    [SerializeField]
    GameObject selectHelpObject;
    [SerializeField]
    GameObject moveHelpObject;

    // int health;

    bool golemActive = false;
    //bool moving = false;
    //bool movingToResource = false;
    GOLEM_STATE golemState = GOLEM_STATE.Idle;

    Vector2 targetPos;
    Vector2 previousResourcePos;
    Resource targetResource;
    ResourceManager.RESOURCE_TYPE targetResourceType;
    ResourceManager.RESOURCE_TYPE resourceBeingCarried
        = ResourceManager.RESOURCE_TYPE.None;
    int amountBeingCarried = 0;

    Slaver attackTarget;

    // separation and attack movement force
    Vector2 pushForce;

    Vector2 moveVector;

    // --------
    // STATS
    // --------
    float maxHp;
    float hp;
    float healthRegen;
    float attack;
    float rof;
    float defence;
    float movementSpeed;
    float resColSpeed;
    int resCap;


    float attackCooldown;

    AudioSource thisAudio;



    // -----------------------------
    // INITIALISATION
    // -----------------------------

    void Awake()
    {
        thisAudio = gameObject.AddComponent<AudioSource>();

        if(!selectHelpShown)
        {
            selectHelpShown = true;
            selectHelpObject.SetActive(true);
        }
    }

    public void Init(float hp, float healthRegen, float attack, float rof, float defence,
        float movementSpeed, float resColSpeed, int resCap)
    {
        this.maxHp = hp;
        this.hp = hp;
        this.healthRegen = healthRegen;
        this.attack = attack;
        this.rof = rof;
        this.defence = defence;
        this.movementSpeed = movementSpeed;
        this.resColSpeed = resColSpeed;
        this.resCap = resCap;

        this.attackCooldown = 1.0f / this.rof;

        healthDisplay.SetMaxHealth(maxHp);
    }



    // -----------------------------
    // GET / SET
    // -----------------------------

    public void ActivateGolem()
    {
        golemActive = true;
    }


    // -----------------------------
    // UPDATE
    // -----------------------------

    

    // -----------------------------
    // MOUSE EVENTS
    // -----------------------------

    public void OnPointerEnter(PointerEventData eventData)
    {
        InfoTextDisplay.Instance.DisplayText(this.ToString());
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //InfoTextDisplay.Instance.DisplayText("");
    }

    // -----------------------------
    // OVERRIDES
    // -----------------------------

    public override string ToString()
    {
        float baseHP = GolemConstructor.BASE_HP;
        float baseRegen = GolemConstructor.BASE_HP_REGEN;
        float baseAttack = GolemConstructor.BASE_ATTACK;
        float baseRof = GolemConstructor.BASE_ROF;
        float baseDef = GolemConstructor.BASE_DEFENCE;
        float baseMove = GolemConstructor.BASE_MOVEMENT_SPEED;
        float baseColSpeed = GolemConstructor.BASE_RES_COL_SPEED;
        float baseResCap = GolemConstructor.BASE_RES_CAP;

        return "Golem\n" +
            "Max HP: " + baseHP + " (+" + (maxHp - baseHP) + ")       " +
            "Regen: " + baseRegen + " (+" + (healthRegen - baseRegen) + ")       " +
            "Attack: " + baseAttack + " (+" + (attack - baseAttack) + ")       " +
            "Attack Rate: " + baseRof + " (+" + (rof - baseRof) + ")       " +
            "Defence: " + baseDef + " (+" + (defence - baseDef) + ")       " +
            "Move Speed: " + baseMove + " (+" + (movementSpeed - baseMove) + ")       " +
            "Resource Collect Speed: " + baseColSpeed + " (+" + (resColSpeed - baseColSpeed) + ")       " +
            "Resource Capacity: " + baseResCap + " (+" + (resCap - baseResCap) + ")";
    }




    // -----------------------------
    // CONTROLS
    // -----------------------------

    void Update()
    {
        if(golemState != GOLEM_STATE.Idle && golemState != GOLEM_STATE.Attacking)
        {
            float moveAmount = Time.deltaTime * movementSpeed;
            moveVector = Vector2.zero;

            switch(golemState)
            {
                case GOLEM_STATE.MovingToPosition:
                    MovingToPosition(moveAmount);
                    break;

                case GOLEM_STATE.MovingToResource:
                    MovingToResource(moveAmount);
                    break;
                    
                case GOLEM_STATE.TakingResourceToBase:
                    TakingResourceToBase(moveAmount);
                    break;

                case GOLEM_STATE.MovingToEnemy:
                    MovingToEnemy(moveAmount);
                    break;
            }
            
            if (moveVector.magnitude > moveAmount)
            {
                moveVector.Normalize();
                moveVector *= moveAmount;
            }

            transform.position += (Vector3)moveVector;
        }

        // push force stuff
        transform.position += (Vector3)pushForce * Time.deltaTime;
        pushForce *= Mathf.Max(0, 1 - Time.deltaTime * PUSH_DECAY);

        // draw layer
        float thisY = transform.position.y;
        float t = (thisY - DRAW_START_POS) / DRAW_MAP_SIZE;
        int layer = LAYER_START + (LAYER_COUNT - (int)(t * LAYER_COUNT));
        visualSprite.sortingOrder = layer;

        // health regen
        hp = Mathf.Clamp(hp + healthRegen * Time.deltaTime, 0, maxHp);
        healthDisplay.SetHealth(hp);
    }


    // -----------------------------
    // UPDATE - STATES
    // -----------------------------

    void MovingToPosition(float moveAmount)
    {
        moveVector = targetPos - (Vector2)transform.position;

        if (moveVector.magnitude < PICKUP_RANGE &&
            !resourceHelpShown)
        {
            resourceHelpShown = true;

            // find a resource
            Resource nearbyResource = ResourceManager.Instance.FindResourceNearPosition(
            ResourceManager.RESOURCE_TYPE.Salvage, transform.position);

            // move the help to the resource
            moveHelpObject.SetActive(true);
            moveHelpObject.transform.position = nearbyResource.transform.position;

            // move camera to resource
            CameraRTS.Instance.MoveToPos(1, nearbyResource.transform.position);
        }
    }

    void MovingToResource(float moveAmount)
    {
        // if the resource no longer exists
        if (!targetResource)
        {
            FindNearbyResource(targetPos);
        }

        // if the resource is still there
        if (targetResource)
        {
            moveVector = (Vector2)targetResource.transform.position - (Vector2)transform.position;

            // if within range of the resource
            if (moveVector.magnitude < PICKUP_RANGE)
            {
                // drop current resource if it is different
                if (resourceBeingCarried != ResourceManager.RESOURCE_TYPE.None
                    && resourceBeingCarried != targetResource.GetResourceType())
                {
                    for (int i = 0; i < amountBeingCarried; i++)
                    {
                        Vector2 randomPos = transform.position;
                        randomPos.x += Random.Range(-1.0f, 1.0f);
                        randomPos.y += Random.Range(-1.0f, 1.0f);

                        ResourceManager.Instance.CreateResource(
                            resourceBeingCarried, randomPos);
                    }

                    amountBeingCarried = 0;
                }

                // pickup new resource if there is capacity for it
                if (amountBeingCarried < resCap)
                {
                    previousResourcePos = targetResource.transform.position;
                    resourceBeingCarried = targetResource.GetResourceType();
                    resourceCarrying.enabled = true;
                    // resourceCarrying.color = Resource.RESOURCE_COLOURS[(int)resourceBeingCarried];
                    resourceCarrying.sprite = Resources.Load<Sprite>("ResourceIcons/" +
                        resourceBeingCarried.ToString());

                    targetResource.RemoveResource();
                    targetResource = null;

                    amountBeingCarried++;
                }


                // if there is room for more
                if (amountBeingCarried < resCap)
                {
                    FindNearbyResource(targetPos);

                    // if this new resource is different, go back to back
                    if(targetResource == null ||
                       targetResource.GetResourceType() != resourceBeingCarried)
                    {
                        golemState = GOLEM_STATE.TakingResourceToBase;
                    }
                }
                // otherwise, go to base
                else
                {
                    golemState = GOLEM_STATE.TakingResourceToBase;
                }

                //Debug.Log(amountBeingCarried);
            }

        }
    }

    void TakingResourceToBase(float moveAmount)
    {
        targetPos = Vector2.zero;
        moveVector = targetPos - (Vector2)transform.position;

        if (moveVector.magnitude < PICKUP_RANGE)
        {
            // drop the current resource in the base
            ResourceManager.Instance.AddResource(resourceBeingCarried, amountBeingCarried);

            resourceCarrying.enabled = false;
            resourceBeingCarried = ResourceManager.RESOURCE_TYPE.None;
            amountBeingCarried = 0;

            // find a nearby resource to the previous resource
            FindNearbyResource(previousResourcePos);
        }
    }

    void MovingToEnemy(float moveAmount)
    {
        if (attackTarget)
        {
            targetPos = attackTarget.transform.position;
            moveVector = targetPos - (Vector2)transform.position;

            // if within range, attack the enemy
            if (moveVector.magnitude < ATTACK_RANGE)
                StartCoroutine(AttackTarget());
        }
        else
        {
            golemState = GOLEM_STATE.Idle;

            // find a nearby resource to the previous resource
            FindNearbyResource(previousResourcePos);
        }
    }

    


    // -----------------------------
    // UPDATE
    // -----------------------------

    // attacks target
    // TO DO if they're in range
    IEnumerator AttackTarget()
    {
        attackTarget.TakeDamage(attack, this);
        golemState = GOLEM_STATE.Attacking;
        yield return new WaitForSeconds(attackCooldown);

        if(golemState == GOLEM_STATE.Attacking)
            golemState = GOLEM_STATE.MovingToEnemy;
    }

    void FindNearbyResource(Vector2 searchPos)
    {
        // Find similar resource near the previous resource position
        Resource nearbyResource = ResourceManager.Instance.FindResourceNearPosition(
            targetResourceType, searchPos);

        if (nearbyResource)
        {
            targetResource = nearbyResource;
            targetPos = targetResource.transform.position;

            golemState = GOLEM_STATE.MovingToResource;

            //Debug.Log("Moving to other resource");
        }
        else
        {
            // TODO if failed, find similar resource anywhere

            golemState = GOLEM_STATE.Idle;
        }
    }

    public override void Selected()
    {
        if (selection)
            selection.enabled = true;

        selectHelpObject.SetActive(false);

        if(!moveHelpShown)
        {
            moveHelpShown = true;
            moveHelpObject.SetActive(true);
        }
    }

    public override void Deselected()
    {
        if(selection)
            selection.enabled = false;
    }

    public override void ClickAtPosition(Vector2 clickWorldPos)
    {
        golemState = GOLEM_STATE.MovingToPosition;
        targetResource = null;
        targetPos = clickWorldPos;
        
        moveHelpObject.SetActive(false);
    }

    public override void ClickAtEnemy(Slaver targetEnemy)
    {
        golemState = GOLEM_STATE.MovingToEnemy;
        attackTarget = targetEnemy;

        moveHelpObject.SetActive(false);
    }

    public override void ClickAtResource(Resource targetResource)
    {
        golemState = GOLEM_STATE.MovingToResource;
        this.targetResource = targetResource;
        this.targetResourceType = targetResource.GetResourceType();
        this.targetPos = targetResource.transform.position;

        resourceHelpShown = true;
        moveHelpObject.SetActive(false);
    }

    public override void ClickAtBase()
    {
        if(resourceBeingCarried != ResourceManager.RESOURCE_TYPE.None)
            golemState = GOLEM_STATE.TakingResourceToBase;
    }


    public void TakeDamage(float amount, Slaver slaverAttacking)
    {
        if (golemState != GOLEM_STATE.Dead)
        {
            hp -= amount - defence;

            thisAudio.PlayOneShot(
                Resources.Load<AudioClip>("Sounds/Liz Hit_1"), 0.5f);

            if (hp <= 0)
            {
                hp = 0;

                thisAudio.PlayOneShot(
                Resources.Load<AudioClip>("Sounds/Golem Die_" + Random.Range(1, 4)), 0.5f);

                golemState = GOLEM_STATE.Dead;
                Invoke("Die", 1.2f);
            }
            else
            {
                healthDisplay.SetHealth(hp);

                Vector2 push = transform.position - slaverAttacking.transform.position;
                if (push == Vector2.zero)
                    push = Vector2.one;
                push.Normalize();
                push *= ATTACK_PUSH;
                pushForce = push;
            }
        }
    }

    void Die()
    {
        CommandHandler.Instance.RemoveFromSelection(this);
        GolemSpawner.Instance.RemoveGolem(this);
    }

}
