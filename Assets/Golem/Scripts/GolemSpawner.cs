﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Spawns the initial golem and the findable golems
/// </summary>
public class GolemSpawner : MonoBehaviour
{

    // -----------------------------
    // INSTANCING
    // -----------------------------

    private static GolemSpawner _instance;
    public static GolemSpawner Instance
    {
        get
        {
            if (!_instance)
                _instance = GameObject.FindObjectOfType<GolemSpawner>();

            return _instance;
        }
        private set { }
    }

    // -----------------------------
    // CONSTANTS
    // -----------------------------

    public const string GOLEM_PATH = "Golem";
    
    Golem golemPrefab;

    List<Golem> liveGolemList = new List<Golem>();
    List<Golem> inactiveGolemList = new List<Golem>();

    void Awake()
    {
        golemPrefab = Resources.Load<Golem>(GOLEM_PATH);
    }

    void Start()
    {
        //CreateLiveGolem(new Vector2(2, 0));
        //CreateLiveGolem(new Vector2(3, 0));
        //CreateLiveGolem(new Vector2(4, 0));

        int[] maxList = new int[ResourceManager.RESOURCE_COUNT];
        for (int i = 0; i < maxList.Length; i++)
            maxList[i] = 10;
        GolemConstructor.Instance.CreateGolem(new Vector2(6, 0), maxList);
    }

    public Golem CreateLiveGolem(Vector2 position)
    {
        Golem newGolem = (Golem) Instantiate(golemPrefab, position, Quaternion.identity);
        newGolem.ActivateGolem();
        liveGolemList.Add(newGolem);

        return newGolem;
    }

    public void RemoveGolem(Golem targetGolem)
    {
        liveGolemList.Remove(targetGolem);

        DestroyObject(targetGolem.gameObject);
        Destroy(targetGolem);
    }

    // go through golems and find one inside the rectangle
    public void GetGolemsInArea(Vector2 minCorner, Vector2 maxCorner, ref List<Commandable> golemList)
    {
        Collider2D[] colliderList = Physics2D.OverlapAreaAll(minCorner, maxCorner, CommandHandler.SELECTABLE_LAYER);

        for(int i = 0; i < colliderList.Length; i++)
        {
            Golem golemFound = colliderList[i].GetComponentInParent<Golem>();
            if(golemFound)
            {
                golemFound.Selected();
                golemList.Add(golemFound);
            }
        }
    }

}
