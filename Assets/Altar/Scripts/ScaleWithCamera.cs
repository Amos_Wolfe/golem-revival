﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Scales a world UI element with the camera
/// </summary>
public class ScaleWithCamera : MonoBehaviour
{

    public const float BASE_SCALE = 0.1f;

    void Update()
    {
        float newScale = BASE_SCALE * Camera.main.orthographicSize;
        transform.localScale = Vector3.one * newScale;
    }

}
