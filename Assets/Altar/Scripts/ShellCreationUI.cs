﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShellCreationUI : MonoBehaviour
{

    [SerializeField]
    Text buttonText;




    void Update()
    {
        if (ResourceManager.Instance.
            MeetsRequirements(ResourceManager.GOLEM_SHELL_MATERIAL_REQUIREMENT,
            true))
        {
            buttonText.text = "Create";
        }
        else
        {
            buttonText.text = "Create \n(Not enough materials)";
        }
    }

}
