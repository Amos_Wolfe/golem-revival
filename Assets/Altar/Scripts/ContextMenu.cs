﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Pops open a context menu, and returns the option selected.
/// 
/// TODO closest window if clicked outside the context menu
/// </summary>
public class ContextMenu : MonoBehaviour
{

    // -----------------------------
    // INSTANCING
    // -----------------------------

    private static ContextMenu _instance;
    public static ContextMenu Instance
    {
        get
        {
            if (!_instance)
                _instance = GameObject.FindObjectOfType<ContextMenu>();

            return _instance;
        }
        private set { }
    }



    [SerializeField]
    RectTransform window;

    bool displaying = false;
    Vector2 displayPos;

    public void DisplayAt(Vector2 worldPos)
    {
        displaying = true;
        displayPos = worldPos;

        window.gameObject.SetActive(true);

        UpdateWindow();
    }

    void Update()
    {
        if(displaying)
        {
            UpdateWindow();
        }
    }

    void UpdateWindow()
    {
        Vector2 screenPos = Camera.main.WorldToScreenPoint(displayPos);
        window.offsetMin = screenPos;
        window.offsetMax = screenPos;
    }

}
