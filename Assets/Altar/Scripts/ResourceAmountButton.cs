﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Allows the player to change the amount of a resource for a ritual.
/// </summary>
public class ResourceAmountButton : MonoBehaviour
{

    public const int MIN_AMOUNT = 0;
    public const int MAX_AMOUNT = 10;


    [SerializeField]
    Text amountText;

    [SerializeField]
    Image iconImage;

    int currentAmount = 0;
    ResourceManager.RESOURCE_TYPE resourceType;

    public void Init(ResourceManager.RESOURCE_TYPE resourceType)
    {
        this.resourceType = resourceType;

        // set the icon image
        iconImage.sprite = Resources.Load<Sprite>("ResourceIcons/" +
        resourceType);
    }

    void OnEnable()
    {
        // reclamp amount
        if (currentAmount > ResourceManager.Instance.GetResourceAmount(resourceType))
            currentAmount = ResourceManager.Instance.GetResourceAmount(resourceType);

        amountText.text = currentAmount.ToString();
    }

    public void MaxAmount()
    {
        currentAmount = Mathf.Min(ResourceManager.Instance.GetResourceAmount(resourceType), MAX_AMOUNT);
        amountText.text = currentAmount.ToString();
    }

    public void ChangeResource(int amount)
    {
        currentAmount += amount;

        if (currentAmount > ResourceManager.Instance.GetResourceAmount(resourceType))
            currentAmount = ResourceManager.Instance.GetResourceAmount(resourceType);

        currentAmount = Mathf.Clamp(currentAmount, MIN_AMOUNT, MAX_AMOUNT);
        
        amountText.text = currentAmount.ToString();
    }

    public int GetResourceAmount()
    {
        return currentAmount;
    }

    public ResourceManager.RESOURCE_TYPE GetResourceType()
    {
        return resourceType;
    }

}
