﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Creates a list of resource buttons based on the current resources used.
/// </summary>
public class CreateResourceButtons : MonoBehaviour
{

    public const string RESOURCE_BUTTON_PATH = "ResourceAmountButton";


    // TODO place this in a separate ritualUI script
    //[SerializeField]
    //Text buttonText;


    List<ResourceAmountButton> resourceButtonList = new List<ResourceAmountButton>();

    void Awake()
    {
        int golemResourceCount = 0;
        for(int i = 0; i < ResourceManager.GOLEM_RESOURCES.Length; i++)
        {
            if (ResourceManager.GOLEM_RESOURCES[i])
                golemResourceCount++;
        }

        float buttonHeight = 1.0f / (float)golemResourceCount;
        float buttonY = 0;
        RectTransform buttonPrefab = Resources.Load<RectTransform>(RESOURCE_BUTTON_PATH);

        for(int i = 1; i < ResourceManager.RESOURCE_COUNT; i++)
        {
            if (ResourceManager.GOLEM_RESOURCES[i])
            {
                RectTransform newButton = (RectTransform)Instantiate(buttonPrefab);
                newButton.transform.parent = this.transform;
                newButton.transform.localScale = Vector3.one;

                newButton.anchorMin = new Vector2(0, buttonY);
                newButton.anchorMax = new Vector2(1, buttonY + buttonHeight);
                newButton.offsetMin = Vector2.zero;
                newButton.offsetMax = Vector2.zero;

                ResourceAmountButton newResourceButton = newButton.GetComponent<ResourceAmountButton>();
                newResourceButton.Init((ResourceManager.RESOURCE_TYPE)i);
                resourceButtonList.Add(newResourceButton);

                buttonY += buttonHeight;
            }
        }
    }

    public void MaxAllResources()
    {
        for(int i = 0; i < resourceButtonList.Count; i++)
        {
            resourceButtonList[i].MaxAmount();
        }
    }

    public void SpendResources()
    {
        for (int i = 0; i < resourceButtonList.Count; i++)
        {
            ResourceManager.Instance.SpendResource(resourceButtonList[i].GetResourceType(),
             resourceButtonList[i].GetResourceAmount());
        }
    }

    public int[] GetResourceAmounts()
    {
        int[] resourceAmounts = new int[ResourceManager.RESOURCE_COUNT];

        for(int i = 0; i < resourceButtonList.Count; i++)
        {
            int resIndex = (int)resourceButtonList[i].GetResourceType();
            resourceAmounts[resIndex] = resourceButtonList[i].GetResourceAmount();
        }

        return resourceAmounts;
    }

    /*
    void Update()
    {
        if(MeetsRequirements())
        {
            buttonText.text = "Start Ritual";
        }
        else
        {
            buttonText.text = "Start Ritual \n(Not enough materials)";
        }
    }
    */

        /*
    // TODO place in resource manager
    public bool MeetsRequirements()
    {
        // check if all resources are at least 1
        for (int i = 1; i < ResourceManager.RESOURCE_COUNT; i++)
        {
            if (ResourceManager.Instance.GetResourceAmount((ResourceManager.RESOURCE_TYPE)i) == 0)
            {
                return false;
            }
        }

        return true;
    }
    */

}
