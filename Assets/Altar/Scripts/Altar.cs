﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Altar : MonoBehaviour, IPointerClickHandler
{

    [SerializeField]
    GameObject shellCreationUI;

    [SerializeField]
    GameObject ritualUI;

    [SerializeField]
    SpriteRenderer golemConstructing;

    [SerializeField]
    Animator golemAnim;

    [SerializeField]
    GameObject exclamationMark;

    [SerializeField]
    CreateResourceButtons createResourceButtons;

    bool shellCreated = false;
    bool doingRitual = false;

    AudioSource thisAudio;

    void Awake()
    {
        thisAudio = gameObject.AddComponent<AudioSource>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == 0 && !doingRitual)
        {
            // show shell window
            if (!shellCreated)
            {
                shellCreationUI.SetActive(true);
            }
            // show ritual window
            else
            {
                ritualUI.SetActive(true);
            }
        }
    }

    public void CloseUI()
    {
        shellCreationUI.SetActive(false);
        ritualUI.SetActive(false);
    }

    public void StartBuildingShell()
    {
        if(!shellCreated)
        {
            if(ResourceManager.Instance.
                MeetsRequirements(ResourceManager.GOLEM_SHELL_MATERIAL_REQUIREMENT, true))
            {
                Debug.Log("Create shell");
                shellCreated = true;
                golemConstructing.enabled = true;

                // spend resources
                for(int i = 1; i < ResourceManager.RESOURCE_COUNT; i++)
                {
                    if (ResourceManager.GOLEM_RESOURCES[i])
                    {
                        ResourceManager.Instance.SpendResource((ResourceManager.RESOURCE_TYPE)i,
                            ResourceManager.GOLEM_SHELL_MATERIAL_REQUIREMENT);
                    }
                }

                CloseUI();
            }
            else
            {
                Debug.Log("Not enough resources to create shell");
            }
        }
    }

    public void StartRitual()
    {
        if (shellCreated && !doingRitual)
        {
            exclamationMark.SetActive(false);
            doingRitual = true;
            StartCoroutine(RitualCoroutine());
        }
        else
        {
            Debug.LogError("No shell created");
        }
    }

    // perform the sequence of events in the ritual
    IEnumerator RitualCoroutine()
    {
        Debug.Log("Start Ritual");

        CloseUI();

        yield return new WaitForSeconds(0.1f);

        AdaptiveMusic.Instance.LowerMusic();

        // show animation and wait
        golemAnim.SetTrigger("StartRitual");
        thisAudio.PlayOneShot(Resources.Load<AudioClip>("Sounds/Chant"));

        yield return new WaitForSeconds(4.0f);

        

        // play final sound and wait
        thisAudio.PlayOneShot(Resources.Load<AudioClip>("Sounds/SubsonicSound"));
        yield return new WaitForSeconds(1.2f);

        thisAudio.PlayOneShot(Resources.Load<AudioClip>("Sounds/Boom"));

        GolemConstructor.Instance.CreateGolem( (Vector2)golemAnim.transform.position,
            createResourceButtons.GetResourceAmounts());

        golemConstructing.enabled = false;
        createResourceButtons.SpendResources();
        // GolemSpawner.Instance.CreateLiveGolem((Vector2)transform.position);
        shellCreated = false;
        doingRitual = false;
    }

}
