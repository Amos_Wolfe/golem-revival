﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Shows the requirements for the golem shell structure
/// </summary>
public class GolemShellRequirementText : MonoBehaviour
{

    [SerializeField]
    bool golemResourcesOnly = false;
    
    public int materialRequirement = 5;

    Text requirementsText;

    void Awake()
    {
        requirementsText = GetComponent<Text>();

        string requirementsString = "Requirements: \n";

        for (int i = 1; i < ResourceManager.RESOURCE_COUNT; i++)
        {
            if (!golemResourcesOnly || ResourceManager.GOLEM_RESOURCES[i])
            {
                requirementsString += (ResourceManager.RESOURCE_TYPE)i + ": " +
                    materialRequirement + "\n";
            }
        }

        requirementsText.text = requirementsString;
    }

}
