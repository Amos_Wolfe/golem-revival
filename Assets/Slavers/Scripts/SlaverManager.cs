﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Creates slavers on the edges of the screen.
/// </summary>
public class SlaverManager : MonoBehaviour
{
    // -----------------------------
    // INSTANCING
    // -----------------------------

    private static SlaverManager _instance;
    public static SlaverManager Instance
    {
        get
        {
            if (!_instance)
                _instance = GameObject.FindObjectOfType<SlaverManager>();

            return _instance;
        }
        private set { }
    }
    

    // -----------------------------
    // CONSTANTS
    // -----------------------------

    // TODO global constants class
    public const float MAP_SIZE = 50;

    public const float WAVE_INTERVAL = 50;
    public const float SUSPENSE_TIME = 5;
    public const float SPAWN_INTERVAL = 2;


    public const int SLAVER_COUNT_START = 1;
    public const int SLAVER_COUNT_INCREASE = 1;

    public const string SLAVER_PATH = "Slaver";

    // -----------------------------
    // VARIABLES
    // -----------------------------

    Slaver slaverPrefab;
    bool dangerMusic = false;
    bool suspenseMusic = false;
    List<Slaver> slaverList = new List<Slaver>();

    int currentSlaverCount = SLAVER_COUNT_START;

    // -----------------------------
    // INITIALISATION
    // -----------------------------

    void Awake()
    {
        slaverPrefab = Resources.Load<Slaver>(SLAVER_PATH);
    }

    void Start()
    {
        StartCoroutine(SlaverUpdate());
    }


    // -----------------------------
    // UPDATE
    // -----------------------------

    void Update()
    {
        if(dangerMusic)
        {
            if(slaverList.Count == 0)
            {
                dangerMusic = false;

                if(!suspenseMusic)
                    AdaptiveMusic.Instance.SetMusicState(AdaptiveMusic.MUSIC_STATE.Idle);
            }
        }
    }

    IEnumerator SlaverUpdate()
    {
        while(true)
        {
            yield return new WaitForSeconds(WAVE_INTERVAL - SUSPENSE_TIME);
            AdaptiveMusic.Instance.SetMusicState(AdaptiveMusic.MUSIC_STATE.Suspense);
            yield return new WaitForSeconds(SUSPENSE_TIME);
            suspenseMusic = true;

            AdaptiveMusic.Instance.SetMusicState(AdaptiveMusic.MUSIC_STATE.Danger);
            suspenseMusic = false;
            dangerMusic = true;

            for (int i = 0; i < currentSlaverCount; i++)
            {
                SpawnSlaver();
                yield return new WaitForSeconds(SPAWN_INTERVAL);
            }

            currentSlaverCount += SLAVER_COUNT_INCREASE;
        }
    }

    void SpawnSlaver()
    {
        Vector2 randomSpawn = new Vector2(Random.Range(-1.0f, 1.0f),
            Random.Range(-1.0f, 1.0f));

        randomSpawn.Normalize();
        randomSpawn *= (MAP_SIZE / 2) + 15;

        Slaver newSlaver = (Slaver)Instantiate(slaverPrefab, randomSpawn, Quaternion.identity);
        slaverList.Add(newSlaver);
    }

    public void RemoveSlaver(Slaver targetSlaver)
    {
        slaverList.Remove(targetSlaver);
        DestroyObject(targetSlaver.gameObject);
        Destroy(targetSlaver);
    }

}
