﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Goes to the village and kidnaps people, then travels back outside the map.
/// </summary>
public class Slaver : MonoBehaviour
{

    // -----------------------------
    // CONSTANTS
    // -----------------------------

    public enum SLAVER_STATE { MovingToBase, Fleeing, MovingToGolem, Attacking, Dead };

    public const float MAP_SIZE = 100;

    public const float MOVE_SPEED = 2;
    public const float MOVE_SPEED_CARRYING = 1.5f;

    public const float ATTACK_COOLDOWN = 0.5f;
    public const int ATTACK_DAMAGE = 5;
    public const float ATTACK_RANGE = 0.5f;

    public const float GRAB_RANGE = 1;

    public const float ATTACK_PUSH = 10;
    public const float PUSH_DECAY = 10;

    // -----------------------------
    // VARIABLES
    // -----------------------------

    [SerializeField]
    float maxHealth = 100;

    [SerializeField]
    HealthDisplay healthDisplay;

    [SerializeField]
    SpriteRenderer personCarrying;

    float health;

    bool carryingPerson = false;
    Vector2 startPos;

    Golem attackTarget;

    SLAVER_STATE slaverState = SLAVER_STATE.MovingToBase;

    // separation and attack movement force
    Vector2 pushForce;

    AudioSource thisAudio;


    // -----------------------------
    // INITIALISATION
    // -----------------------------

    void Awake()
    {
        health = maxHealth;
        healthDisplay.SetMaxHealth(maxHealth);
        thisAudio = gameObject.AddComponent<AudioSource>();
    }

    void Start()
    {
        startPos = transform.position;
    }




    // -----------------------------
    // UPDATE
    // -----------------------------

    void Update()
    {
        float moveAmount = Time.deltaTime * MOVE_SPEED;
        Vector2 moveVector = Vector2.zero;

        switch(slaverState)
        {
            case SLAVER_STATE.MovingToBase:
                moveVector = -transform.position;

                if(moveVector.magnitude < GRAB_RANGE)
                {
                    carryingPerson = true;
                    personCarrying.enabled = true;
                    slaverState = SLAVER_STATE.Fleeing;
                    // remove person
                    ResourceManager.Instance.SpendResource(ResourceManager.RESOURCE_TYPE.People, 1);
                }
                break;

            case SLAVER_STATE.Fleeing:
                moveVector = startPos - (Vector2)transform.position;
                
                if(moveVector.magnitude < GRAB_RANGE)
                {
                    DestroyObject(this.gameObject);
                    Destroy(this);
                }
                break;

            case SLAVER_STATE.MovingToGolem:
                if(attackTarget)
                {
                    moveVector = (Vector2)attackTarget.transform.position - 
                        (Vector2)transform.position;

                    // if within range, attack the enemy
                    if (moveVector.magnitude < ATTACK_RANGE)
                        StartCoroutine(AttackTarget());
                }
                else
                {
                    if (carryingPerson)
                        slaverState = SLAVER_STATE.Fleeing;
                    else
                        slaverState = SLAVER_STATE.MovingToBase;
                }
                break;
        }


        if (moveVector.magnitude > moveAmount)
        {
            moveVector.Normalize();
            moveVector *= moveAmount;
        }

        transform.position += (Vector3)moveVector;
    }

    // attacks target
    // TO DO if they're in range
    IEnumerator AttackTarget()
    {
        attackTarget.TakeDamage(ATTACK_DAMAGE, this);
        slaverState = SLAVER_STATE.Attacking;

        yield return new WaitForSeconds(ATTACK_COOLDOWN);

        if (slaverState == SLAVER_STATE.Attacking)
            slaverState = SLAVER_STATE.MovingToGolem;
    }

    public void TakeDamage(float amount, Golem golemAttacking)
    {
        if (slaverState != SLAVER_STATE.Dead)
        {
            health -= amount;

            thisAudio.PlayOneShot(
                Resources.Load<AudioClip>("Sounds/Hit_" + Random.Range(2, 4)), 0.5f);

            if (health <= 0)
            {
                health = 0;
                // spawn person resource on death (if carrying one)
                if (carryingPerson)
                {
                    ResourceManager.Instance.CreateResource(
                        ResourceManager.RESOURCE_TYPE.People, transform.position);
                }

                thisAudio.PlayOneShot(
                Resources.Load<AudioClip>("Sounds/Liz Squish_" + Random.Range(1, 4)), 0.5f);

                slaverState = SLAVER_STATE.Dead;
                Invoke("Die", 1.2f);

                // remove self
                //DestroyObject(gameObject);
                //Destroy(this);
            }
            else
            {
                healthDisplay.SetHealth(health);

                attackTarget = golemAttacking;
                slaverState = SLAVER_STATE.MovingToGolem;

                Vector2 push = transform.position - golemAttacking.transform.position;
                if (push == Vector2.zero)
                    push = Vector2.one;
                push.Normalize();
                push *= ATTACK_PUSH;
                pushForce = push;
            }
        }
    }

    void Die()
    {
        // let slaver manager know
        SlaverManager.Instance.RemoveSlaver(this);
    }

}
